# Contributing

Everyone is welcome to help. Several ways for that:

- **Develop**. [GitLab](https://gitlab.com/adacraft/) is the main place where
  code is collected, issues are documented, and discussions about code are had.
- **Create**. adacraft is looking for designers, artists, coders, programmers
  to bring your creative and amazing work to show and inspire other people.
- **Document**. Everyone loves documentation. Help is needed porting examples,
  and adding documentation, and creating tutorials.
- **Teach**. Teach a workshop, a class, a friend, a collaborator! Tag
  [@adacraft_org on Twitter](https://twitter.com/adacraft_org) and we will do
  our best to share what you're doing.

A good way to contribute is first reaching out the adacraft team in the [Discord
server](https://www.adacraft.org/discord/).

If you prefer get in touch in private you can also directly contacting me
(Nicolas Decoster). Send me a direct message
[@ogadaki](https://twitter.com/ogadaki) on Twitter, or an email to
[adacraft@mailbox.org](mailto:adacraft@mailbox.org), or ping me on Discord
(ogadaki#8319).

## Hacktoberfest 2022

Here is a special [page about Hacktoberfest 2022 for
adacraft](https://adacraft.notion.site/Hacktoberfest-2022-for-adacraft-8f3db1df845147dd8ed4806ae42da8e7).
It's a good start if you participate to Hacktoberfest, and want to know how to
contribute for adacraft.