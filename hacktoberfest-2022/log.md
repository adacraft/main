# Hacktoberfest 2022 log

## How to trace your nocode contribution ?

For the Hacktoberfest, people can participate with non code contributions. This
means that, in principle, these contributions don't modify the repositories and
can't be tracked. To make it possible and account this contributions for
Hacktoberfest, the idea is to create a small repository modification.

Here is how this is managed for adacraft project.

If you work on a non code issue (like community, documentation, examples, etc.),
after finishing it, add any relevant comment to the issue, discuss it with the
team, and, when it's finished, identify your contribution in this file (log.md).

For that, just add a line in the next section ("Log") with the issue
number and its description.

Example:
* #37 The title of the issue

## Log


