# Collective Statement

Adapted from the [ml5.js community statement](https://ml5js.org/about/).

We are a collective of, and in solidarity with, people from every gender
identity and expression, sexual orientation, race, ethnicity, language,
neuro-type, size, ability, class, religion, culture, subculture, political
opinion, age, skill level, occupation, and background. We acknowledge that not
everyone has the time, financial means, or capacity to actively participate, but
we recognize and encourage involvement of all kinds. We facilitate and foster
access and empowerment. We are all learners.

We like these hashtags: #noCodeSnobs (because we value community over
efficiency), #newKidLove (because we all started somewhere), #unassumeCore
(because we don't assume knowledge), and #BlackLivesMatter (because of course).

**In practice:**


- We are not code snobs. We do not assume knowledge or imply there are things
  that somebody should know.
- We insist on actively engaging with requests for feedback regardless of their
  complexity.
- We welcome newcomers and prioritize the education of others. We strive to
  approach all tasks with the enthusiasm of a newcomer. Because we believe that
  newcomers are just as valuable in this effort as experts.
- We consistently make the effort to actively recognize and validate multiple
  types of contributions.
- We are always willing to offer help or guidance.


**In times of conflict:**

- We listen.
- We clearly communicate while acknowledging other's feelings.
- We admit when we're wrong, apologize, and accept responsibility for our
  actions.
- We are continuously seeking to improve ourselves and our community.
- We keep our community respectful and open.
- We make everyone feel heard.
- We are mindful and kind in our interactions.
